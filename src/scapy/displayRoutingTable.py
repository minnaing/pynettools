import scapy.config
import scapy.route


def displayRoutingTable(listAll = False, filter = True, ):

	strFormat = "%-15s %-15s %-15s %-15s %-15s"
	print  strFormat%("Network", "Netmask", "Gateway", "Interface", "Address")
	for network, netmask, _, interface, address in scapy.config.conf.route.routes:
		print strFormat%(scapy.utils.ltoa(network) , scapy.utils.ltoa(netmask), _, interface, address)

displayRoutingTable()
