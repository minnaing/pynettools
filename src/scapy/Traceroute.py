import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import socket
from scapy.all import traceroute

def tracert(host, echo = True):

	trace, unanswer = traceroute(host_name, verbose=0)
	result = trace.get_trace().values()[0]
	ips = [i[1][0] for i in result.items() ]
	result = []
	i = 1
	for ip in ips:
		node = (i, ip, socket.getfqdn(ip))
		if echo:
			print "%2d. %-15s %-15s" % node
			i = i + 1
		result.append(node)
	return result

print tracert("google.com")