
from scapy.all import * # get_if_list, get_if_hwaddr

class NetInterfaces:
	'''
		Just a wrapper class to create the interface get_list
		collect the information from scapy [conf.route, get_if_list(), get_if_hwaddr(), conf.iface]
	'''
	def __init__(self, only_physical = True, set_default = False): 
		self.IF_LIST = {}
		self._bind_MAC(only_physical);
		self._bind_IP();
		if set_default:
			self._set_default();

	def _bind_MAC(self, only_physical): 
		for i in get_if_list():
			try:
				mac = get_if_hwaddr(i)
				self.IF_LIST[i] = {"mac": mac}
			except:
				if not only_physical:
					self.IF_LIST[i] = None

	def _bind_IP(self):
		r = [x for x in scapy.all.conf.route.routes if x[2] != '0.0.0.0']
		for x in r:
			self.IF_LIST[x[3]]["ip"] = x[4]
			self.IF_LIST[x[3]]["gateway"] = x[2]

	def _set_default(self):
		self.IF_LIST[scapy.config.conf.iface]['default'] = True
	
	def get_all(self): 
		return self.IF_LIST
	
	def get_default(self):
		return self.IF_LIST[scapy.config.conf.iface]

nif = NetInterfaces(True)
l = nif.get_all()
for x in l:
	#print "%-8s :: %s" %(x, l[x])
	print " --- [%s] --- " %x
	for e in l[x].items():
		print "%-8s :: %s" % e



print " === Default === "
print nif.get_default()