import datetime
import socket
import uuid
import sys, os, platform
import urllib2
import json

now = datetime.datetime.now()

class Localhost:
    host = socket.gethostname()
    def __int__(self):
        pass
    def hostname(self):
        return self.host
    def ip(self):
        return socket.gethostbyname(self.host)
    def mac(self):
        return ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])
    def os(self):
        return "%s ( %s, %s, %s )" % ( sys.platform, os.name, platform.system(), platform.release())
    def now(self):
        return now.strftime("%Y-%m-%d %H:%M:%S")
    def user(self):
        if ( sys.platform.lower().find('nix') == 0 ):
            return os.getlogin() # os.getusername() , os.getuid()
        elif( sys.platform.lower().find('win') == 0 ):
            return os.environ['USERNAME'] # =os.getenv('USERNAME')
        else:
            import getpass
            return getpass.getuser()
        
    def script(self):
        # sys.version, platform.python_version(), "The Python version is %s.%s.%s" % sys.version_info[:3], sys.version_info
        return "Python " + platform.python_version()
    
    def wwwID(self):
        try:
            output = ""
            #internetID = json.loads(urllib2.urlopen("http://api.hostip.info/get_json.php").read())
            internetID = json.loads(urllib2.urlopen("http://ipinfo.io/json/").read())
            for key, value in internetID.iteritems() : #Python3 use items() 
                output = output + " %-15s :: %s \n" % (key.replace("_", " ").capitalize(), value)
        except ValueError:
            output = 'No Reault : Fail to Parse JSON.'
        return output

    def bgp(self):
        raise Exception("API doesn't support.")
        try:
            output = ""
            url = "" # Currently API is not working.
            internetID = json.loads(urllib2.urlopen(url % (self.ip())).read())
            for key, value in internetID.iteritems() : #Python3 use items() 
                output = output + " %-15s :: %s \n" % (key.replace("_", " ").capitalize(), value)
        except ValueError:
            output = 'No Reault : Fail to Parse JSON.'
        return output
    # ===
        
def localhost(more = False):
    l = Localhost()
    print " === [ LocalHost Information ] === \n"
    print " Internal IP Address     : " + l.ip()
    print " Host Name               : " + l.hostname()
    print " MAC Address             : " + l.mac()
    print " System Time             : " + l.now()
    print " Operating System        : " + l.os()
    print " Script Runtime Engine   : " + l.script()
    print " Current User            : " + l.user()
    print 
    if (more):
        print " === [ Internet ID ] === \n\n" + l.wwwID()
        #print " === [ BGP Info ] === \n\n" + l.bgp()

if __name__ == "__main__":
    print
    if len(sys.argv) > 1 and sys.argv[1] == 'all':
        localhost(True)
    else:
        localhost(True)
