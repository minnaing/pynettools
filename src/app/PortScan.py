#!/usr/bin/env python

"""
    === [ THE Port Scanner - v 1.0 - Min Naing ] ===
    * Scanning the Open Port of the Destination Host

    Name : Port Scanner
    Author : Han Min Naing
    License : MIT License
    Credit: http://www.coderholic.com/python-port-scanner/
    Hosted : http://minnaing.com/code/python/port-scanner
    Last Modified : 5:07 AM 7/22/2013
    Requirement : Python 2.4 or higher

*** *** ***

    This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

*** *** ***

    Input : HOST NAME like localhost, example.com etc. OR IP ADDRESS like 8.8.8.8
    Output : List of Open Ports OR Verbose
    Option : CLI Mode OR Python Runtime Initiation OR Import as Module
    Setting : TimeOut, Quick Scan
    
    Note : Quick Scan ONLY scan the well-known ports

    Knowledge :
        Total Port Available ~
        ~ serve as Server to Server
        ~ serve as Client to Server
        System Ports (0-1023), User Ports (1024-49151), and the Dynamic and/or Private
        
"""

__author__ = "Han Min Naing"
__version__ = 1.0

from socket import *
from datetime import datetime
import sys
import time

class PortScan:
    TimeOut = 1 # 4 is recommended
    OpenPorts = [] # return
    Scanned = False
        
    _FeaturedPorts = {
        20 : "FTP (Data) - TCP",
        21 : "FTP (Control) - TCP",
        22 : "SSH - TCP",
        23 : "Telnet - TCP",
        25 : "SMTP - TCP",
        49 : "TACACS, XTACACS - UDP",
        57 : "DNS - TCP, UDP",
        67 : "DHCP - TCP",
        68 : "DHCP - TCP",
        69 : "TFTP - TCP",
        80 : "HTTP aka www - TCP",
        110 : "POP3 - TCP",
        135 : "MS DCOM Service Control Manager",
        137 : "NetBIOS - TCP, UDP",
        138 : "NetBIOS - TCP, UDP",
        139 : "NetBIOS - TCP, UDP",
        143 : "IMAP4 - TCP",
        161 : "SNMP - UDP",
        389 : "Lightweight Directory Access Protocol (LDAP)",
        443 : "HTTPS - TCP",
        445 : "Microsoft DS",
        500 : "IPSec - UDP",
        636 : "LDAP, LDAPS - TCP",
        1433 : "SQL - TCP",
        1434 : "SQL - UDP",
        1701 : "L2TP - UDP",
        1723 : "PPTP - TCP",
        3128 : "Squid or Web cashes",
        3306 : "MySQL - TCP",
        3389 : "RDP - TCP",
        6881 : "Bit Torrent",
        8080 : "HTTP Proxy or Alias",
        8888 : "HTTP Proxy or Alias"
        }

    WellKnownPorts = range(0, 1024)
    RegisteredPorts = range(1024, 49151)
    FeaturedPorts = _FeaturedPorts.keys()
    
    def __init__(self, host):
        self.target = host
        self.targetIP = gethostbyname(host)

    def scanPort(self, i):
        # === [ Per Port Scanning ] === 
        s = socket(AF_INET, SOCK_STREAM)
        s.settimeout(self.TimeOut)
        try:
            s.connect((self.targetIP, i))
            self.OpenPorts.append(i)
            return True
        except Exception:
            return False
        finally:
            s.close()

    def now(self):
        now = datetime.now()
        return now.strftime("%Y-%m-%d %H:%M:%S")

    def openPorts(self):
        # === [ Return Open Ports' List ] === 
        if (self.Scanned):
            return self.OpenPorts
        else:
            print(" - Require :: Scan() or verboseScan() - ")

    def Scan(self, Port2Scan = FeaturedPorts ):
        # === [ Silent Scan ] ===
        for i in sorted(Port2Scan):
            self.scanPort(i)
        self.Scanned = True
        
    def verboseScan(self, Port2Scan = FeaturedPorts, displayClosePorts = False ):
        # === [ Verbose Scan ] ===
        startTime = time.time()
        print("="*50)
        print("START scanning \t Host : %s ( %s )" % (self.target, self.targetIP) )
        print("\t \t with '" + str(self.TimeOut) + "s' TimeOut Setting")
        print("\t \t at " + self.now() + "\n")
        for i in sorted(Port2Scan):
            print "Access : %d" % i, " \t ",  # 3to2 :)
            if self.scanPort(i):
                print('* OPEN \t %s' % self._FeaturedPorts[i])
            else :
                print('- Close -')
        self.Scanned = True
        print("="*50)
        print("END of Scanning @ %s ... Elapsed time : %.3fs." % (self.now(), (time.time()-startTime)))
        n = len(self.OpenPorts)
        if (n == 0):
                print("\n === \t [ %s ~ %s :: NO open port. ] \t ===" % (self.target, self.targetIP) )
        elif ( n == 1):
                print("\n === \t [ %s ~ %s :: %d port open. ] \t ===" % (self.target, self.targetIP, n) )
        else:
                print("\n === \t [ %s ~ %s :: %d ports open. ] \t ===" % (self.target, self.targetIP, n) )
        #print(self.OpenPorts)
        print

if __name__ == '__main__':
    if len(sys.argv) > 1:
        target = sys.argv[1]
    else:
        target = raw_input('Enter host to scan: ') # :) 3 to 2
    
    ps = PortScan(target)
    ps.verboseScan(ps.FeaturedPorts)
    
    # *** Custom Port Scan
    # ps.verboseScan([20, 21, 22, 23, 80, 443, 8080, 8888])

    # *** Silent Scan // Good to use as Module
    # ps.Scan(ps.FeaturedPorts)
    # print(ps.openPorts())
    
