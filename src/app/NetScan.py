import os, sys, socket
from IPy import IP
import threading
import time

# The main ARP Module Management
# But it will be better we can use scapy @MsWin
# If MsWin { arp } else { scapy }
if os.name == 'nt':
    # MS Windows
    import arp
else:
    # Unix/Linux Platform
    # We can simply use scapy
    # or
    # import ArpRequest
    raise("Oh! Use scapy. It is faster.")
    pass

# An Optional netaddr Module Management
# Function : MAC to Vendor
try:
    from netaddr import *
    vendorDisplay = True
except:
    vendorDisplay = False


__doc__ = \
'''
    Info :
        Name         : NetScan
        Version      : 0.1
        Author       : Han M Naing ( minnaing163@gmail.com )
        License      : MIT
    For :
        Net Scan, IP Scan, Ping Scan or Ping Sweep
    Require:
        ARP for ARP request to Network (Mandatory for MsWin)
        IPy for IP Lists and CIDR Notation
        Netaddr for MAC -> OUI -> Vendor of Network Interface (Optional)
    Road Map:
        Support Threading
    Usage:
        As Module:
            See main() function
        As CLI:
            python NetScan.py [IP/CIDR] {option}
    Notes:
        *   The goal of this software is to scan all online hosts in the LOCAL network.
        Mostly we use 'scapy', the most powerful framework.
        However, most people are having trouble with install 'scapy' in MS Windows.
        So, I hope this script will be helpful to them.
        *   If netaddr (optional) is installed, the script can provide the Vendor Information.
        The script automatically detect 'netaddr' and set OUI to False at display() while it is not available.
    Knowledge:
        * While NetScan, we can't rely on ping, because
            - some hosts are live but can't ping
            - ping is Layer 3 and need ICMP
            - block by Security System like firewall
'''


__version__ = 0.1
__author__ = 'Han Naing'

# refer http://xael.org/norman/python/python-nmap/
__stdDisplayStr__ = [" - %15s > %18s :: %-18s ( %s )" , " - %15s > %18s :: %s "]

class ScanHost(threading.Thread):
    # Per HOST scanning
    def __init__(self, ip):
        self.ip = str(ip)
    def run(self):
        result = arp.arp_resolve(self.ip, 0)
        mac = arp.mac_straddr(result, 1, ":")
        host = socket.getfqdn(self.ip) #socket.gethostbyaddr(ip)
        return (self.ip, mac, host)        
    def display(self, offline = False, OUI = False):
        ip, mac, host = self.run()
        if mac != '00:00:00:00:00:00' :
            if OUI and vendorDisplay:
                try:
                    eui = EUI(mac)
                    vendor = eui.oui.registration().org
                except:
                    vendor = '-'
                print __stdDisplayStr__[0] % ( ip, mac, host, vendor )
            else:
                print __stdDisplayStr__[1] % ( ip, mac, host )
            return True
        else:
            if offline:
                print " - %15s >   --- OFFLINE --- " % (ip)
            return False
            

class NetScan:
    # Main Handler
    def __init__(self, ipLists):
        self.ipLists = IP(ipLists)
        self.online = []
    def run(self): pass
    def display(self, offline = False, OUI = False):
        print
        print ' === [ SCAN %s START ] === ' % ( self.ipLists.strNormal(3) )
        print
        if OUI and vendorDisplay:
            print __stdDisplayStr__[0] % ( "IP ADDRESS", " - MAC Address - ", " - Host Name - ", " - Vendor -" )
        else:
            print __stdDisplayStr__[1] % ( "IP ADDRESS", " - MAC Address - ", " - Host Name - " )
        for i in range(1, len(self.ipLists)-1):
            if ScanHost(self.ipLists[i]).display(offline, OUI):
                self.online.append(self.ipLists[i])
        print
        print ' === [ SCAN END ~ Total %s Host(s) Online ] === ' % (len(self.online))
    def getOnline(self):
        print self.online

def getIP():
    return socket.gethostbyname(socket.gethostname())


class ScapyNetScan(): pass

def main(ipLists):
    ns = NetScan(ipLists)
    ns.display(True, False)
    print
    # Showing how to get result
    #ns.getOnline()
    #print 
    #print ' ~*~ ' * 10
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        argv = sys.argv[1]
    else:
        argv = raw_input(" Enter Network to Scan : ")
    
    start_time = time.time()
    if argv == 'now':
        #argv = IP(getIP()).make_net('255.255.255.0')
        argv = IP(getIP()).make_net('255.255.255.240')
       
    try:
        #ipLists = IP(argv)
        ipLists = IP('192.168.1.0/27')
        main(ipLists)
    except:
        print
        print ' --- --- --- --- [ %s ] --- --- --- --- ' % ('NetScan')
        print ' * NetScan ', __version__ , ' by ', __author__ 
        print ' * Usage : python NetScan.py [IP/CIDR] {Option}'
        print ' ---' * 11
    print '-' * 30
    print " Time Escape" , time.time() - start_time, "seconds"
    

