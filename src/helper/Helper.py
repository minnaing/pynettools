import httpagentparser
import socket
    
def user_agent_parser(s):
    return httpagentparser.simple_detect(s)

def ip_to_host(ip): # nslookup
    return socket.gethostbyaddr(ip)[0]

def host_to_ip(host):
    return socket.gethostbyname(host)