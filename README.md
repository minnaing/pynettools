PyNetTools
---

Name : PyNetTools   
Version : 0.0.1   
Description : I found out that when people want to get Network Related result they often execute shell command and parse the string. I think there is a better way. As Python has banch of useful Networking related libraries, I will use them to generate the similar result   
   
So far planning:   
- ping   
- nslookup   
- traceroute   
- arp   
- ListInterfaces   
- PortScan   
- NetScan // aka LAN Scan, Neighbour Discovery